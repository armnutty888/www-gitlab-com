---
layout: handbook-page-toc
title: "Phishing Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Phishing Program

The GitLab phishing program provides ongoing phishing simulations and trainings to GitLab team members that enhances knowledge and identification of malicious emails as well as helps identify social engineering vulnerabilties where additional education is provided.  Phishing simulations are provided by PhishLabs, GitLab's contracted solution that will help satisfy external regulatory requirements and bolster customer assurance.

## Objective

To provide phishing simulations and trainings to a subset of team members within the GitLab organization, at miminum on a quarterly cadence.

## When will phishing simulations occur?

At minimum, one phishing simulation will take place each fiscal quarter.  

Prior to the phishing simulation taking place, a general notification to the GitLab organization will be posted to the `#whats-happening-at-gitlab` Slack channel (the exact date/time and team members receiving the email will not be communicated).

## Who will receive the phishing simulations?

A subset of the GitLab organization will receive an email from our phishing vendor, PhishLabs. At minimum, every team member who has access to the production systems that represent or support `gitlab.com` will receive a phishing simulation at lease once per fiscal year.

## How do we select who will receive the phishing simulations?

Phishing simulations are an integral part of our overall security awareness education. Selection is based on a role-based approach which allows us to tailor our trainings towards the appropriate skills and knowledge to be effective for those roles. This helps GitLab deliver the right training to the right people at the right time.

## Who all is involved?

The Security Compliance team coordinates with the following teams before a phishing simulation exercise:

* Security Incident Response Team (SIRT)
   * Responsible for triaging phishing emails
   * If they are made aware of a simulation they will provide the team member with additional instructions

* IT Operations/Team Member Enablement
   * Owner of GSuite (GitLab's email platform)

## What will a phishing simulation email look like?

The phishing simulation email from Phishlabs will appear as though it is originating from GitLab or a fictitious company. This email will look realistic/authentic in the attempt to engage the team member to click a link. We would love to show you what this looks like but that would defeat the purpose of the test.

### Why it is important to complete the post-simulation training

We want everyone to be as successful as possible and these quick trainings will guide and provide a few tips to help recognize malicious emails in the future.  The trainings are quick 5 minute interactive videos and completing them will help us with future training assessments.

### Phishing email is successful

In the event the link is clicked, the team member will be redirected to a landing page notifying them they failed the simulation. This action will automatically enroll the end user in a short training session also provided by PhishLabs and a link to the training will be sent to the team member to complete.

### Simulation Result Outcomes

| Action                                 | Outcome |
| -------------------------------------- | ------- |
| Submitted email to phishing@gitlab.com | Passed - followed the [handbook process](/handbook/security/#what-to-do-if-you-suspect-an-email-is-a-phishing-attack) for reporting suspected phishing emails |
| Did nothing with the email             | Passed - took no action within 3 business days  |
| Clicked on the link                    | Failed, training will be assigned  |

#### Training Module

Our phishing partner, PhishLabs, curates and hosts the training modules which will be assigned upon falling for the phishing simulation. The training is designed to reinforce and provide real world examples.  We highly encourage you to complete the training soon after being received as this will help to reinforce and better prepare you to spot phishing attempts in the future.  The training modules are short - up to 5 minutes in length and will be coming from <no-reply@phishlabs.com>

If the training is not completed, a reminder will be sent to complete the training. If required, we will communicate incomplete assigned training modules to managers for assistance with completion.  Demonstration of a completed training supports compliance with the Phishing program and will strengthen our regulatory requirements.

### Phishing email is not successful

We sincerly hope all participants identify the phishing simulation test and do one of the following:

* Forward the phishing email to `phishing@gitlab.com` per the [handbook instructions](/handbook/security/#what-to-do-if-you-suspect-an-email-is-a-phishing-attack)
    * If the email you forward is from the phishing simulation exercise, you will receive a reply congratulating you on identifying the simulation phishing email and requesting you to please not tell anyone else until after the exercise has come to an end.
* Engage the Security team on the #security Slack channel
    * A member of the SIRT team will engage you with a general reply in channel.  Additionally, you will receive a direct message thanking you for notifying the team and request that you please not tell anyone else until after the exercise has come to an end.
 * Do nothing and delete the email


### Phishing Simulation and Training Metrics

The Security Compliance team will initiate and track the quarterly phishing simulation exercises in the GitLab ZenGRC instance.  Once the exercise has completed, the Security Compliance team will provide non-identifying results in the [Phishing Program](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/phishing) project.

The SIRT team will track exercises where they are engaged with issues in the [SIRT Issue Tracker](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/issues) whilst keeping the results confidential. They will share the results with Security Compliance for metrics and reporting purposes. 

Metrics and reporting from PhishLabs will be ingested into Snowflake - TBD

We will be utilizing Sisense to create dashboards for reporting to organization - TBD

### Additional Resources

* [How to identify a basic phishing attack](https://about.gitlab.com/handbook/security/#how-to-identify-a-basic-phishing-attack)
* [PhishLabs](https://about.gitlab.com/handbook/business-ops/tech-stack/#phishlabs)

### Questions and Answers

*I fell for the phishing simulation email, what do I do?*

* Please complete the assigned training module you have been assigned as soon as possible. Please do not discuss this with anyone else as it may skew the results of the phishing exercise.

*I didn't fall for the phishing simulation email, what do I do?*

* Please forward the email to phishing@gitlab.com using the [instructions in the handbook.](https://about.gitlab.com/handbook/security/#what-to-do-if-you-suspect-an-email-is-a-phishing-attack). Please do not discuss with anyone else as it may skew the results of the phishing exercise.

*I use a physical Yubikey for multifactor authentication, why did I still fail the phishing simulation?*

* While a physical Yubikey or other token device used will prevent you from fully being phished, not all GitLab systems support these devices.  As this simulation exercise is for the training and awareness the goal is to test how GitLab team-members respond to these emails.

*I thought the Red Team was conducting the phishing exercises?*

* Correct! The Red Team was conducting our phishing exercises with a tool they built but unfortunatly Google began identifying the exercises as malicious and were shutting them down quicker with each attempt.  This caused the phishing exercises to not be beneficial along with no results to work with.

*Why are we using an external vendor?*

* Due to our internally created solution being identified as malicious by Google, we needed to utilize an external solution that would be able to provide a consistent process that we could rely on.  With the assistance of the Red Team, their recommendation resulted in our partnership with PhishLabs.

*Who decides who receives these phishing simulations?*

* The Security Compliance team runs the phishing program and will engage the organization, at minimum once per quarter, to identify and select team members to receive phishing simulations to help protect the company, satisfy GitLab security controls, regulatory requirements and customer assurance.

*Why was I chosen?*

* All GitLab team members can be included in our phishing simulation exercises. There are certain prerequisites we must, at minimum, satisfy like team members with access to the GitLab production environments, but security is everyones responsibility at GitLab. To help keep everyone aware and vigilant you may receive a phishing simulation.

*How often will I receive these?*

* If you receive a phishing simulation, it will most likely be once or possibly twice a year. If you were to fail a simulation you may be selected to receive an additional simulation to validate if the training is working as intended.

*I don't want to be included, how do I remove myself?*

* All GitLab team members have the responsiblity to help keep themselves, team members, the company and the customer secure; all have the opportunity to receive a phishing simulation.

*Is this an invasion of privacy?*

* The phishing program is coverd by the [Employee Privacy Policy](https://about.gitlab.com/handbook/legal/privacy/employee-privacy-policy/)

*Will I be publicly shamed?*

* No, we will never post or create metrics which will associate a team member success or failure with a phishing simulation exercise.  We will generate non-identifying metrics to be shared internally and public-facing.

*I never fall for the phishing simulations received, why am I still receiving these simulation emails?*

* Unfortunatly, phishing emails continue to improve in deception and the ability to look genuine.  By receiving phishing simulations your ability to spot the real thing is an invaluable skill to help protect yourself, team members, GitLab, and the customer.

*How can I provide Feedback on my experience?*

* We will be kicking off a feedback process once the phishing simulation testing has completed.

### Additional Questions, Comments, Concerns?

Please reach out to the [Security Compliance team!](/handbook/engineering/security/security-assurance/security-compliance/compliance.html)
